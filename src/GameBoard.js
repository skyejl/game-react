import React from 'react';
import './GameBoard.less';
import ResultList from "./ResultList";

class GameBoard extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            result: []
        }
        this.submitAnswer = this.submitAnswer.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    submitAnswer() {
        this.props.submitNumbers(this.state.result);
    }
    handleInputChange(event) {
        let num = event.target.value;
        this.setState({
            result: [...this.state.result, num]
        })
    }
    render() {
        return(
            <div className="gameBoard">
                <form action="">
                    <fieldset className="inputField">
                        <legend className="legendFeild">Guess Number</legend>
                        <input maxLength="1" className="number" type="text" onChange={this.handleInputChange}/>
                        <input maxLength="1" className="number" type="text" onChange={this.handleInputChange}/>
                        <input maxLength="1" className="number" type="text" onChange={this.handleInputChange}/>
                        <input maxLength="1" className="number" type="text" onChange={this.handleInputChange}/>
                    </fieldset>
                </form>
                <ResultList></ResultList>
                <button className="submitButton" onClick={this.submitAnswer}>Good Luck</button>
            </div>
        );
    }
}
export default GameBoard;
